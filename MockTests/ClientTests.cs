﻿using MockPractice;
using Moq;
using NUnit.Framework;

namespace MockTests
{
    [TestFixture]
    public class ClientTests
    {
        Client client;
        Mock<IService> mockService;
        Mock<IContentFormatter> contentFormatter;

        [SetUp]
        public void Setup()
        {
            mockService = new Mock<IService>();
            contentFormatter = new Mock<IContentFormatter>();
            mockService.Setup(l => l.Connect());
            mockService.Setup(l => l.Dispose());
            mockService.Setup(l => l.GetContent(It.IsAny<long>())).Returns("someString");
            mockService.Setup(l => l.Name).Returns("myName");
            contentFormatter.Setup(l => l.Format(It.IsAny<string>())).Returns("something");
            client = new Client(mockService.Object, contentFormatter.Object);
        }


        [Test]
        public void GetServiceName_Should_ReturnServiceName()
        {
            string result = client.GetServiceName();
            Assert.AreEqual(mockService.Object.Name, result);
        }

        [Test]
        public void GetIndentityFormatted_Should_ReturnFormattedString()
        {
            var result = client.GetIdentityFormatted();
            StringAssert.StartsWith(("<formatted>"), result);
            StringAssert.EndsWith(("</formatted>"), result);
        }

        [Test]
        public void ContentFormatter_Should_RunOnce()
        {
            var result = client.GetContent(15);
            mockService.Verify(l => l.GetContent(15), Times.Once);

        }

        [Test]
        public void GetContentFormatted_Should_ReturnFormattedContent()
        {
            string result = client.GetContentFormatted(5);
            Assert.AreEqual("something", result);
        }

        [Test]
        public void ContentFormatter_Should_NotRun()
        {
            mockService.Setup(l => l.IsConnected).Returns(true);

            var result = client.GetContent(10);
            mockService.Verify(l => l.Connect(), Times.Never);

        }

        [Test]
        public void IfNotConnected_Should_Connect()
        {
            mockService.Setup(l => l.IsConnected).Returns(false);
            var result = client.GetContent(9);
            mockService.Verify(e => e.Connect(), Times.Once);
        }

    }
}
